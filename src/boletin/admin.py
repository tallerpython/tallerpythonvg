from django.contrib import admin

# Register your models here.
from boletin.models import *
from .forms import *


class AdminRegistrado(admin.ModelAdmin):
        list_display = ["nombre", "email", "fecha"]
        form = RegModelForm  # AGREGAMOS EL MODEL FORM
        # list_display_links = ["email"]
        list_editable = ["email"]
        list_filter = ["fecha"]
        search_fields = ["nombre", "email"]
        # class Meta:
        # model = Registrado


admin.site.register(Registrado, AdminRegistrado)