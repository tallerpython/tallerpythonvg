from django.shortcuts import render
from .forms import RegForm # Importamos el formulario RegForm desde esta ruta'./forms'
from .models import Registrado
# Create your views here.
#'def inicio(request):'
#return render(request,"inicio.html",{})

# Create your views here.
def inicio(request):
    form = RegForm(request.POST or None)  # Agregamos el método POST --> request.POST
    # print (dir(form)) # Permite mostrar en consola los métodos que podemos invocarcon 'form'
    contexto = {
        "el_formulario":
            form,
    }
    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get("nombre")
        email2 = form_data.get("email")
        objeto = Registrado.objects.create(nombre=nombre2, email=email2)
        # OTRA FORMA DE GUARDAR LOS OBJETOS:
        # objeto = Registrado()
        # objeto.nombre = nombre2
        # objeto.email = email2
        # objeto.save()

    return render(request, "inicio.html", contexto) # Cambiamos {} por el nombre deldiccionario creado 'contexto'
