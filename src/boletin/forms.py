from django import forms
from .models import Registrado
class RegForm(forms.Form):
    nombre = forms.CharField(max_length=100)
    email = forms.EmailField()
class RegModelForm(forms.ModelForm):
    class Meta:
        modelo = Registrado
        campos = ["nombre", "email"]
    def clean_email(self):
        email = self.cleaned_data.get("email")
        email_base,proveedor = email.split("@")
        dominio,extension = proveedor.split(".")
        if not extension == "edu":
            raise forms.ValidationError("Solo se aceptan correos con extensión.EDU")
        return email
